from tkinter import *
import math

a=str()
b=str()

def okno4(event):


  def ochistka1(event):
    ent1.delete(0, END)

  def help1():
    win = Toplevel(root)
    win.title("Помощь")
    win.minsize(width=200, height=150)
    lab = Label(win,text='Это тестовая версия калькулятора 2 -\n введите выражение без знака "=" \n и нажмите кнопку "Рассчитать"')
    lab.grid(row=0, column=0, sticky=W)

  def raschet(event):
    global b
    b=ent1.get()
    ent1.delete(0,END)
    ent1.insert(END, b)
    ent1.insert(END, '=')
    ent1.insert(END,eval(b))

  win = Toplevel(root)
  win.title("Простой калькулятор")

  m = Menu(win)
  win.config(menu=m)
  hm = Menu(m)
  m.add_command(label="Помощь", command=help1)

  ent1 = Entry(win, width=30)
  ent1.bind("<Return>", raschet)
  ent1.bind("<Double-Button-1>", ochistka1)

  but1 = Button(win, text="Рассчет")
  but1.bind("<Button-1>", raschet)

  ent1.grid(row=0, column=0, sticky=W + E)
  but1.grid(row=1, column=0, sticky=W + E)


def okno1(event):
  a = str()
  b = 0

  def ochistka1(event):
    ent1.delete(0, END)

  def help1():
    win = Toplevel(root)
    win.title("Помощь")
    win.minsize(width=200, height=150)
    lab = Label(win,
                text='Это тестовая версия калькулятора -\n e^x и logX не работают в этой версии')  # метка (просто текст)
    lab.grid(row=0, column=0, sticky=W)

  def sum(event):
    global a
    global b
    a += ent1.get()
    a += '+'
    ent1.delete(0, END)
    # ent1.insert(END, a)

  def razn(event):
    global a
    global b
    a += ent1.get()
    a += '-'
    b = eval(a)
    a = b
    ent1.delete(0, END)

  def umn(event):
    global a
    global b
    a += ent1.get()
    a += '*'
    b = eval(a)
    a = b
    ent1.delete(0, END)

  def delit(event):
    global a
    global b
    a += ent1.get()
    a += '/'
    ent1.delete(0, END)

  def ravno(event):
    global a
    a += ent1.get()
    ent1.delete(0, END)
    ent1.insert(END, a)
    ent1.insert(END, '=')
    ent1.insert(END, eval(a))
    del a
    # b=eval(a)

  def step(event):
    global a
    a += ent1.get()
    a += '**'
    ent1.delete(0, END)

  def e(event):
    ent1.delete(0, END)
    ent1.insert(END, math.e)

  def pi(event):
    ent1.delete(0, END)
    ent1.insert(END, math.pi)

  win = Toplevel(root)
  win.title("Простой калькулятор")

  m = Menu(win)
  win.config(menu=m)
  hm = Menu(m)
  m.add_command(label="Помощь", command=help1)

  but1 = Button(win, text="e")
  but1.bind("<Button-1>", e)

  but2 = Button(win, text="pi")
  but2.bind("<Button-1>", pi)

  but3 = Button(win, text="+")
  but3.bind("<Button-1>", sum)

  but4 = Button(win, text="-")
  but4.bind("<Button-1>", razn)

  but5 = Button(win, text="*")
  but5.bind("<Button-1>", umn)

  but6 = Button(win, text="/")
  but6.bind("<Button-1>", delit)

  but7 = Button(win, text="^")
  but7.bind("<Button-1>", step)

  but8 = Button(win, text="e^x")
  but8.bind("<Button-1>", )

  but9 = Button(win, text="logX")
  but9.bind("<Button-1>", )

  but10 = Button(win, text="=")
  but10.bind("<Button-1>", ravno)

  ent1 = Entry(win, width=30)
  # ent1.bind("<Button-1>", ochistka1)

  ent1.grid(row=0, columnspan=3, sticky=W + E)
  but1.grid(row=1, column=0, sticky=W + E)
  but2.grid(row=1, column=1, sticky=W + E)
  but3.grid(row=2, column=0, sticky=W + E)
  but4.grid(row=2, column=1, sticky=W + E)
  but5.grid(row=3, column=0, sticky=W + E)
  but6.grid(row=3, column=1, sticky=W + E)
  but7.grid(row=4, column=0, sticky=W + E)
  but8.grid(row=4, column=1, sticky=W + E)
  but9.grid(row=5, columnspan=2, sticky=W + E)
  but10.grid(row=1, column=2, rowspan=5, sticky=W + E + N + S)



def okno3(event):
  def ochistka1(event):
    ent1.delete(0, END)

  def sin(event):
    znach = math.radians(float(ent1.get()))
    rez = math.sin(znach)
    ent1.delete(0, END)
    ent1.insert(END, rez)

  def cos(event):
    znach = math.radians(float(ent1.get()))
    rez = math.cos(znach)
    ent1.delete(0, END)
    ent1.insert(END, rez)

  def tg(event):
    znach = math.radians(float(ent1.get()))
    rez = math.tan(znach)
    ent1.delete(0, END)
    ent1.insert(END, rez)

  def ctg(event):
    znach = math.radians(float(ent1.get()))
    rez = 1 / math.tan(znach)
    ent1.delete(0, END)
    ent1.insert(END, rez)

  def asin(event):
    znach = float(ent1.get())
    rez = math.degrees(math.asin(znach))
    ent1.delete(0, END)
    ent1.insert(END, rez)

  def acos(event):
    znach = float(ent1.get())
    rez = math.degrees(math.acos(znach))
    ent1.delete(0, END)
    ent1.insert(END, rez)

  def atg(event):
    znach = float(ent1.get())
    rez = math.degrees(math.atan(znach))
    ent1.delete(0, END)
    ent1.insert(END, rez)

  win = Toplevel(root)
  win.title("Квадратные уравнения")

  m = Menu(win)
  win.config(menu=m)
  hm = Menu(m)
  m.add_command(label="Помощь", command=help3)

  but1 = Button(win, text="sin")
  but1.bind("<Button-1>", sin)

  but2 = Button(win, text="cos")
  but2.bind("<Button-1>", cos)

  but3 = Button(win, text="tg")
  but3.bind("<Button-1>", tg)

  but4 = Button(win, text="ctg")
  but4.bind("<Button-1>", ctg)

  but5 = Button(win, text="asin")
  but5.bind("<Button-1>", asin)

  but6 = Button(win, text="acos")
  but6.bind("<Button-1>", acos)

  but7 = Button(win, text="atg")
  but7.bind("<Button-1>", atg)

  ent1 = Entry(win, width=30)
  ent1.bind("<Button-1>", ochistka1)

  ent1.grid(row=0, columnspan=2, sticky=W + E)
  but1.grid(row=1, column=0, sticky=W + E)
  but2.grid(row=2, column=0, sticky=W + E)
  but3.grid(row=3, column=0, sticky=W + E)
  but4.grid(row=4, column=0, sticky=W + E)
  but5.grid(row=1, column=1, sticky=W + E)
  but6.grid(row=2, column=1, sticky=W + E)
  but7.grid(row=3, column=1, sticky=W + E)






def okno2(event): #Функция вывода второго окна, тоже что и основное(почти)

  def ochistka1(event):
    ent1.delete(0, END)

  def ochistka2(event):
    ent2.delete(0, END)

  def ochistka3(event):
    ent3.delete(0, END)

  def p(event):
    a = float(ent1.get())
    b = float(ent2.get())
    c = float(ent3.get())
    D = (b * b) - (4 * a * c)
    if (b * b) - (4 * a * c) < 0:
      ent5.delete(0, END)
      ent5.insert(END, "нет корней")
      ent6.delete(0, END)
      ent6.insert(END, "нет корней")
    else:
      x1 = (((-b) + (((b * b) - (4 * a * c)) ** (1 / 2))) / (2 * a))
      x2 = (((-b) - (((b * b) - (4 * a * c)) ** (1 / 2))) / (2 * a))
      ent5.delete(0, END)
      ent5.insert(END, x1)
      ent6.delete(0, END)
      ent6.insert(END, x2)
    ent4.delete(0, END)
    ent4.insert(END, D)

  win = Toplevel(root)
  win.title("Квадратные уравнения")

  m = Menu(win)  # создается объект Меню на главном окне
  win.config(menu=m)
  hm = Menu(m)
  m.add_command(label="Помощь", command=help2)

  labA = Label(win, text='a=')
  labB = Label(win, text='b=')
  labC = Label(win, text='c=')
  labD = Label(win, text='D=')
  labX1 = Label(win, text='x1=')
  labX2 = Label(win, text='x2=')

  ent1 = Entry(win, width=30)
  ent1.bind("<Button-1>", ochistka1)
  ent2 = Entry(win, width=30)
  ent2.bind("<Button-1>", ochistka2)
  ent3 = Entry(win, width=30)
  ent3.bind("<Button-1>", ochistka3)
  ent4 = Entry(win, width=30)
  ent5 = Entry(win, width=30)
  ent6 = Entry(win, width=30)

  but = Button(win, text="Решить")
  but.bind('<Button-1>', p)

  ent1.grid(row=0, column=1, sticky=W + E)
  labA.grid(row=0, column=0, sticky=W + E)
  ent2.grid(row=1, column=1, sticky=W + E)
  labB.grid(row=1, column=0, sticky=W + E)
  ent3.grid(row=2, column=1, sticky=W + E)
  labC.grid(row=2, column=0, sticky=W + E)
  ent4.grid(row=3, column=1, sticky=W + E)
  labD.grid(row=3, column=0, sticky=W + E)
  ent5.grid(row=4, column=1, sticky=W + E)
  labX1.grid(row=4, column=0, sticky=W + E)
  ent6.grid(row=5, column=1, sticky=W + E)
  labX2.grid(row=5, column=0, sticky=W + E)
  but.grid(columnspan=2,sticky=W + E)

def help():
  win=Toplevel(root)
  win.title("Помощь")
  win.minsize(width=200,height=150)
  lab = Label(win, text='Для началаработы -\n выберете пункт меню.')  # метка (просто текст)
  lab.grid(row=0, column=0, sticky=W)

def about():
  win = Toplevel(root)
  win.title("Помощь")
  win.minsize(width=200, height=150)
  lab = Label(win, text='Эта программа создана для\n демонстрации способностей')  # метка (просто текст)
  lab.grid(row=0, column=0, sticky=W)

def help2():
  win = Toplevel(root)
  win.title("Помощь")
  win.minsize(width=200, height=150)
  lab = Label(win, text='Введите значения а, b, c.\nЗатем нажмите кнопку "Решить"')  # метка (просто текст)
  lab.grid(row=0, column=0, sticky=W)

def help3():
  win = Toplevel(root)
  win.title("Помощь")
  win.minsize(width=200, height=150)
  lab = Label(win, text='Введите значение угла в градусах,\n либо значение функции,\nзатем выберете функцию')  # метка (просто текст)
  lab.grid(row=0, column=0, sticky=W)






root=Tk() #создание основного окна
root.title("Калькулятор")

m = Menu(root)  # создается объект Меню на главном окне
root.config(menu=m)  # окно конфигурируется с указанием меню для него

hm = Menu(m)
m.add_command(label="Помощь",command=help)
m.add_command(label="О программе",command=about)

but1=Button(root,text="простой калькулятор")#кнопка
but1.bind("<Button-1>",okno1)

but2=Button(root,text="квадратные уравнения")#кнопка
but2.bind("<Button-1>",okno2)

but3=Button(root,text="тригонометрические функции")#кнопка
but3.bind("<Button-1>",okno3)

but4=Button(root,text="Калькулятор 2")#кнопка
but4.bind("<Button-1>",okno4)

but1.grid(row=1,column=0,sticky=W+E)
but2.grid(row=2,column=0,sticky=W+E)
but3.grid(row=3,column=0,sticky=W+E)
but4.grid(row=4,column=0,sticky=W+E)

root.mainloop()